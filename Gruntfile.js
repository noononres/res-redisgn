/**
 * Created by noonon on 8/4/15.
 */

module.exports = function (grunt) {
    grunt.initConfig({
        connect: {
            server: {
                options: {
                    port: 8001,
                    hostname: 'localhost',
                    open: true,
                    keepalive: true,
                    index: 'index.html',
                    base: ['.']
                }
            }
        },
        webfont: {
            res_icons: {
                src: 'public/icons/*.svg',
                dest: 'public/fonts',
                destCss: 'public/css',
                options: {
                    font: 'res_icons',
                    fontFilename: 'res_icons',
                    types: 'ttf',
                    autoHint: false,
                    htmlDemo: false

                }
            }
        },

        watch: {
            font: {
                files: ['/public/icons/*.svg'],
                tasks: ['webfont'],
                options: {
                    livereload: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-webfont');

    grunt.registerTask('default', ['connect', 'watch']);
};